package com.atlassian.marshalling.protobuf;

import com.atlassian.annotations.PublicApi;
import com.atlassian.marshalling.api.Marshaller;
import com.atlassian.marshalling.api.MarshallingException;
import com.atlassian.marshalling.api.MarshallingPair;
import com.atlassian.marshalling.api.Unmarshaller;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.Parser;

import static java.util.Objects.requireNonNull;

/**
 * Optimised implementation for Protocol Buffer generated objects.
 *
 * @since 1.0.0
 */
@PublicApi
public class ProtobufMarshalling<T extends MessageLite> implements Marshaller<T>, Unmarshaller<T> {
    private final Parser<T> parser;

    public ProtobufMarshalling(Parser<T> parser) {
        this.parser = requireNonNull(parser);
    }

    @Override
    public byte[] marshallToBytes(T obj) throws MarshallingException {
        return obj.toByteArray();
    }

    @Override
    public T unmarshallFrom(byte[] bytes) throws MarshallingException {
        try {
            return parser.parseFrom(bytes);
        } catch (InvalidProtocolBufferException e) {
            throw new MarshallingException("Unable to parse", e);
        }
    }

    /**
     * @return a {@link MarshallingPair} using the supplied {@link Parser}.
     */
    public static <T extends MessageLite> MarshallingPair<T> pair(Parser<T> parser) {
        final ProtobufMarshalling<T> sm = new ProtobufMarshalling<>(parser);
        return new MarshallingPair<>(sm, sm);
    }
}
